# markdown-taha


 <img src="https://tabaneshahr.com/wp-content/uploads/2020/06/Logo_Horizontal-300x76.png" align="right" style="height: 64px"/>

# First project Tabanshahr

* **Taha Namdar**

  

![photo](https://png.pngtree.com/thumb_back/fh260/background/20190223/ourmid/pngtree-pure-color-watercolor-graffiti-gradient-background-board-design-board-design-image_66713.jpg) 
  
* خیلی **خوشحالم** که با `تابان شهر `  همکاری میکنم

 

### Bold and Italic
This text **is bold**.  
This text *is italic*.  
This text ~~is struck out~~.

### Header Text
# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6


 

 

### Links
  `[text](link)` sytnax:

[tabanshahr](https://tabaneshahr.com/)

 

```html

 [tabanshahr](https://tabaneshahr.com/)
 
```


<!--convert to html-->
<!--convert to html-->
<!--convert to html-->

<br><hr>

 
 <p>  <img src="https://tabaneshahr.com/wp-content/uploads/2020/06/Logo_Horizontal-300x76.png" align="right" style="height: 64px"/></p>
<h1 id="first-project-tabanshahr">First project Tabanshahr</h1>
<ul>
<li><strong>Taha Namdar</strong></li>
</ul>
<p><img src="https://png.pngtree.com/thumb_back/fh260/background/20190223/ourmid/pngtree-pure-color-watercolor-graffiti-gradient-background-board-design-board-design-image_66713.jpg" alt="photo"> </p>
<ul>
<li>خیلی <strong>خوشحالم</strong> که با <code>تابان شهر</code>  همکاری میکنم</li>
</ul>
<h3 id="bold-and-italic">Bold and Italic</h3>
<p>This text <strong>is bold</strong>.<br>This text <em>is italic</em>.<br>This text <del>is struck out</del>.</p>
<h3 id="header-text">Header Text</h3>
<h1 id="header-1">Header 1</h1>
<h2 id="header-2">Header 2</h2>
<h3 id="header-3">Header 3</h3>
<h4 id="header-4">Header 4</h4>
<h5 id="header-5">Header 5</h5>
<h6 id="header-6">Header 6</h6>
<h3 id="links">Links</h3>
<p>  <code>[text](link)</code> sytnax:</p>
<p><a href="https://tabaneshahr.com/">tabanshahr</a></p>
<pre><code class="lang-html">
 [<span class="hljs-string">tabanshahr</span>](<span class="hljs-link">https://tabaneshahr.com/</span>)
</code></pre>
